import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' },
  { path: 'login', loadChildren: ()=> import('./pages/auth/config/auth.module').then(a => a.AuthModule) },
  { path: 'dashboard', loadChildren: ()=> import('./pages/dashboard/config/dashboard.module').then(d => d.DashboardModule) },
  { path: 'customers', loadChildren: ()=> import('./pages/customers/config/customers.module').then(c => c.CustomersModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
