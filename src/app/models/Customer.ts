import { Addresses } from "./Addresses";

export class Customer {
    id!: number;
    name!: string;
    lastname!: string;
    phone!: string;
    email!: string;
    addresses!: Addresses[];
  }