import { Customer } from "./Customer";

export class Addresses {
    id!: number;
    customer: Customer = new Customer;
    address!: string;
  }