
export class OrderDetail {
    idOrder!: number;
    idProduct!: number;
    title!: string;
    quantity!: number;
    price!: number;
    image!: string;
}