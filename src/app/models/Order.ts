import { OrderDetail } from "./OrderDetail";

export class Order{
    id!: number;
    idClient!: number;
    sendAddress!:string;
    date!: string;
    orderDetail!: OrderDetail
}