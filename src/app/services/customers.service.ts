import { Injectable } from '@angular/core';
import { Addresses } from '../models/Addresses';
import { Customer } from '../models/Customer';
import { HttpService } from './HttpService/http.service';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private http: HttpService){

  }

  findAllCustomers(){
    return this.http.get<Customer[]>(`/api/customers/`,{});
  }

  saveCustomer(customer: Customer){
    return this.http.post<Customer>(`/api/customers/`, customer,{});
  }


  saveAddress(address: Addresses){
    return this.http.post<Addresses>(`/api/addresses/`, address,{});
  }


}
