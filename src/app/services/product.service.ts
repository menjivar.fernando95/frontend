import { Injectable } from '@angular/core';
import { Product } from '../models/Product';
import { HttpService } from './HttpService/http.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpService){

  }

  getAllProducts(){
    return this.http.get<Product[]>(`/api/products/`,{});
  }
}
