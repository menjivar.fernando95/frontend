import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpService } from '../HttpService/http.service';
import { User } from 'src/app/models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  currentUserSubject: BehaviorSubject<any>;
  currentUser: Observable<any>;

  constructor(private http: HttpService, private route: Router){
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser') || '{}'));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): any {
    return this.currentUserSubject.value;
  }

  isAuthenticate(): boolean{
    const helper = new JwtHelperService();
    let token = null;
    console.log('observable authenticate',this.currentUserValue);
    if(this.currentUserValue){
      token = this.currentUserValue.token;
    }
    if(token){
      return !helper.isTokenExpired(token);
    }
    return false;

  }

  login(username: string, password: string) {
    return this.http.postLogin<User>(`/auth/login`, { username, password },{})
        .pipe(map((user:User) => {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
        }));
}

  checkToken(){
    return this.http.get(`/sgst-ws-api-gateway/ws/checkToken`, {});
  }

  logout(){
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.route.navigate(['/login']);
  }

}
