import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  
  apiUrl: string;

  constructor(private httpService: HttpClient){
      this.apiUrl = environment.apiUrl;
  }

  get<result>(path: string, options: object){
      return this.httpService.get<result>(this.apiUrl + path, options);
  }

  post<result>(path: string, data: any, options: object){
      return this.httpService.post<result>(this.apiUrl + path, data, options);
  }

  postLogin<result>(path: string, data: any, options: object){
    return this.httpService.post<result>("http://localhost:8115" + path, data, options);
}

  update(path: string, data: any, options: object){
    return this.httpService.put(this.apiUrl + path, data, options);
  }

  getByUrl<result>(url: string, options: object){
    return this.httpService.get<result>(url, options);
  }

}
