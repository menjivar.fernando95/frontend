import { Injectable } from '@angular/core';
import { Order } from '../models/Order';
import { OrderDetail } from '../models/OrderDetail';
import { HttpService } from './HttpService/http.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpService){

  }

  findAllOrders(){
    return this.http.get<Order[]>(`/api/order/`,{});
  }

  saveOrder(order: Order){
    return this.http.post<Order>(`/api/order/`, order,{});
  }


  saveOrderDetail(orderDetail: OrderDetail[]){
    return this.http.post<OrderDetail[]>(`/api/order/detail`, orderDetail,{});
  }

}
