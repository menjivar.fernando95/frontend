import { Component } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent {

  updateTableCustomer: boolean = false;

  updateTable(value : string){
    console.log('se emitio el event updatetable', this.updateTableCustomer);
    this.updateTableCustomer = !this.updateTableCustomer;
      console.log('se se actualizo', this.updateTableCustomer)
  }

}
