import { NgModule } from '@angular/core';
import {AuthGuard as Guar} from 'src/app/helpers/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from '../customers.component';

const routes: Routes = [
  { path: '', component: CustomersComponent, canActivate:[Guar] },
];




@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)],
    exports: [RouterModule]
  
})
export class CustomersRoutingModule { }
