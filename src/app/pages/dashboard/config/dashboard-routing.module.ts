import { NgModule } from '@angular/core';
import {AuthGuard as Guar} from 'src/app/helpers/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../dashboard.component';
import { BuyComponent } from 'src/app/components/buy/buy.component';
const routes: Routes = [
  { path: '', component: DashboardComponent, canActivate:[Guar] },
  { path: 'detail', component: BuyComponent, canActivate:[Guar] },
];




@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)],
    exports: [RouterModule]
  
})
export class DashboardRoutingModule { }
