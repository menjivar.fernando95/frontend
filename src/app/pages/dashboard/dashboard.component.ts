import { Component, OnInit } from '@angular/core';
import { OrderDetail } from 'src/app/models/OrderDetail';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  comprar: boolean = false;

  cart: OrderDetail[]=[];
  reset!: boolean;
  
  constructor(){}
  ngOnInit(): void {
 
  }

  addItem(newItem: OrderDetail[]) {
    this.cart = newItem;
    console.log("emitt", this.cart);
  }

  closeModal(value: string){
    if(value == 'true'){
      console.log('se limpiar el shopingcart', this.reset);
      this.cart = [];
      this.reset = !this.reset;
      console.log('se limpiar el shopingcart', this.reset);
    }
    console.log("Se emitio el evento padre");
    this.comprar=false;
  }

  changeComprar(){
    this.comprar = true;
  }

}
