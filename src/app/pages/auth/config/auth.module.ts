import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from '../auth.component';
import { AuthRoutingModule } from './auth.routing';
import { ComponentsModule } from 'src/app/components/components.module';



@NgModule({
    declarations: [AuthComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        ComponentsModule,
  
    ]
})
export class AuthModule { }
