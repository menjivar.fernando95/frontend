import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { AuthGuard } from './helpers/auth.guard';
import { AuthenticationService } from './services/AuthService/authentication.service';
import { HttpService } from './services/HttpService/http.service';
import {Interceptor} from './helpers/http.interceptor';
import {ErrorInterceptor} from './helpers/Error.interceptor';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ComponentsModule,
    HttpClientModule

  ],
  providers: [
    HttpService,
    AuthenticationService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS,useClass: Interceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
