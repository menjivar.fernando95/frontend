import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Customer } from 'src/app/models/Customer';
import { Order } from 'src/app/models/Order';
import { OrderDetail } from 'src/app/models/OrderDetail';
import { CustomersService } from 'src/app/services/customers.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css']
})
export class BuyComponent implements OnInit {

  @Input()
  cart: OrderDetail[] = [];

  @Output() newItemEvent = new EventEmitter<string>();

  customers: Customer[] = [];

  seleccionado: number = 0;
  selectAddress: string ='';
  errorSaveOrder: boolean = false;
  saveOrder: boolean = false;
  finalizarOrder: boolean = false;

  customer: Customer = {
    id: 0,
    name: '',
    lastname: '',
    phone: '',
    email: '',
    addresses: []
  };

  total: number = 0;

  constructor(private cs: CustomersService, private os: OrderService){
  }
  ngOnInit(): void {
    this.cart.map((p)=>{
      this.total = this.total + p.price;
    });

    this.getAllCustomers();
  }

  closeCart(value: string) {
    console.log("Se emitio el evento hijo");
    this.newItemEvent.emit(value);
  }

  cancelar(value: string){
    this.cart = [];
    this.newItemEvent.emit(value);
    console.log("Se emitio el evento cancelar");
  }

  checkout(){
    let order= new Order();
    if(this.seleccionado > 0 && this.selectAddress != ''){
      order.idClient = this.seleccionado;
      order.sendAddress = this.selectAddress;
  
      this.os.saveOrder(order).subscribe({
        next: (e) =>{
          console.log("Guardando Orden:" + e.id);
          this.cart.map((c)=>{
            c.idOrder = e.id;
          })
        }, error: (error) =>{
          console.log("Ocurrio un error al intentar guardar la orden");
          this.errorSaveOrder = true;
        }, complete: ()=>{
          console.log("cart a guardar", this.cart);
          this.os.saveOrderDetail(this.cart).subscribe({
            next: (e)=>{
              console.log("Guardando la ordenDetail");
            }, error: (error)=>{
              console.log("Error al guardar la ordenDetail");
            }, complete: ()=>{
              console.log("OrdenDetailGuardado con exito");
              this.errorSaveOrder = false;
              this.saveOrder = true;
              this.finalizarOrder = true;
            }
          })
        }
      });
  
    }else{
      console.log("Error, seleccione un cliente y una address");
      this.errorSaveOrder = true;
    }



  }

  
  changeCustomer(){
    console.log("Se selecciono el custoemer:", this.seleccionado);
   this.customers.map((e)=>{
      if(e.id == this.seleccionado){
        console.log("Se encontro el customer");
        this.customer = e;
        console.log("Customer seleccionado:" + this.customer.name);
      }
      
   })
  }

  getAllCustomers(){
    console.log("Consultando customers");
      this.cs.findAllCustomers().subscribe((response: Customer[])=>{
          this.customers = response;
          console.log(response);
      })
  }

}
