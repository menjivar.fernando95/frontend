import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { OrderDetail } from 'src/app/models/OrderDetail';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit, OnChanges {

  @Output() newItemEvent = new EventEmitter<OrderDetail[]>();

  @Input()
  resetCart!: boolean;

  products: Product[] = [];

  shopcart: OrderDetail[] =[];
  orderDetail: OrderDetail = new OrderDetail();

  cantidad: number = 1;

  loadProducts: boolean = true;

  constructor(private ps: ProductService){

  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['resetCart']){
      console.log('reset cart', this.resetCart);
      this.shopcart = [];
    }
  }
  ngOnInit(): void {
    this.getAllProducts();
  }

  addNewItem(value: OrderDetail[]) {
    this.newItemEvent.emit(value);
  }

  getAllProducts(){
    this.ps.getAllProducts().subscribe({
      next:(p: Product[])=>{
        this.products = p;
      }, error: (error) =>{
        console.log("Error al obtener los datos");
      }, complete: ()=>{
        console.log("Consulta finalizada con exito");
        this.loadProducts = false;
      }
    });
  }

  addProduct(id: any, title: any, price: any, quantity: any, image: string){
    this.orderDetail = new OrderDetail();
     this.orderDetail.idProduct = id;
     this.orderDetail.title = title;
     this.orderDetail.quantity = quantity;
     this.orderDetail.price = (price* quantity);
     this.orderDetail.image = image;
     this.shopcart.push(this.orderDetail);
     console.log(this.shopcart);
     this.addNewItem(this.shopcart);

  }

}
