import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Customer } from 'src/app/models/Customer';
import { CustomersService } from 'src/app/services/customers.service';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.css']
})
export class CustomerTableComponent implements OnInit, OnChanges {

  @Input()
  updateTableCustomer: boolean = false;

  ngOnInit(): void {
    this.getAllCustomers();
  }

  constructor(private cs: CustomersService){

  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['updateTableCustomer']){
      console.log("actualizando la tabla", this.updateTableCustomer);
      this.getAllCustomers();
    }
  }

  customers: Customer[] = [];

  getAllCustomers(){
    console.log("Consultando customers");
      this.cs.findAllCustomers().subscribe((response: Customer[])=>{
          this.customers = response;
          console.log(response);
      })
  }

  
}
