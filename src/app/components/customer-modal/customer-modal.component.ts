import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup,FormBuilder, FormControl, Validators } from '@angular/forms';
import { Addresses } from 'src/app/models/Addresses';
import { Customer } from 'src/app/models/Customer';
import { CustomersService } from 'src/app/services/customers.service';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.css']
})
export class CustomerModalComponent implements OnInit {

  @Output() newItemEvent = new EventEmitter<string>();

  private customerId: number = 0;
  formCustomer!: FormGroup;
  formAddresses! : FormGroup;
  customer: Customer = {
    id: 0,
    name: '',
    lastname: '',
    phone: '',
    email: '',
    addresses: []
  };

  address: Addresses = {
    id: 0,
    customer: this.customer,
    address: ''
  }

  next: boolean = true;
  errorSaveCustomer: boolean = false;
  errorSaveAddress: boolean = false;
  

  constructor(private fb: FormBuilder,private customerService: CustomersService){}

  updateCustomers(value: string) {
    this.next = true;
    console.log("Se emitio updatecustomers con valor:");
    this.newItemEvent.emit(value);
   
  }

  ngOnInit(): void {
    this.formCustomer = this.fb.group({
      name: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['',Validators.required]
    });

    this.formAddresses = this.fb.group({
      customerId: [this.customerId, Validators.required],
      address:['',Validators.required]
    });
  }

   

 


  saveCustomer(){
    for (const i in this.formCustomer.controls) {
      this.formCustomer.controls[i].markAsDirty();
      this.formCustomer.controls[i].updateValueAndValidity();

    }
    if(this.formCustomer.valid){
      console.log("Guardando el customer..."+ this.formCustomer.get('name')?.value);
      this.customer = new Customer();
      this.customer.name = this.formCustomer.get('name')?.value;
      this.customer.lastname = this.formCustomer.get('lastName')?.value;
      this.customer.phone = this.formCustomer.get('phone')?.value;
      this.customer.email = this.formCustomer.get('email')?.value;
     this.customerService.saveCustomer(this.customer).subscribe({
      next: (c: Customer)=>{
        this.customer = c;
      }, error: (error) =>{
        console.log("Error no se guardo el customer");
        this.errorSaveCustomer = true;
      }, complete: () =>{
        console.log("Proceso terminado" + Object.values(this.customer));
        this.next = false;
      }
     })
    }
    
  }

  saveAddresses(){
    console.log("Guardando la address..."+ this.formCustomer.get('address')?.value);
    this.address = new Addresses();
    this.address.customer.id = this.customer.id;
    this.address.address = this.formAddresses.get('address')?.value;
    this.customerService.saveAddress(this.address).subscribe({
      next: (a: Addresses)=>{
      }, error: (error)=>{
        console.log("Error no se guardo la address");
        this.errorSaveAddress = true;
      },
      complete: ()=>{
        console.log("Proceso terminado" + Object.values(this.address));
      
      }
    })
  }

}
