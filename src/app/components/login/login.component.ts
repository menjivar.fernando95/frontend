import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/AuthService/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  validateForm!: FormGroup;
  errorMessage = '';
  error = false;

  constructor(private fb: FormBuilder, private auth: AuthenticationService, private route: Router) {
    // redirect to home if already logged in
    if (this.auth.currentUserValue) {
      this.route.navigate(['/']);
  }
  }

  submitForm(): void {
    console.log("Intentando authenticar");
     for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();

    }

    if(this.validateForm.valid){
     this.auth.login(
        this.validateForm.get('username')?.value,
        this.validateForm.get('password')?.value
      ).subscribe((user:any) =>{
        if(user.code === '00'){
          this.route.navigate(['dashboard']);
        }
        else if(user.code === '400'){
          this.errorMessage = user.message;
          this.error = true;
        }
      });

    }



  }


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }



}
