import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { CustomerTableComponent } from './customer-table/customer-table.component';
import { CustomersService } from '../services/customers.service';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CustomerModalComponent } from './customer-modal/customer-modal.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductService } from '../services/product.service';
import { BuyComponent } from './buy/buy.component';
import { OrderService } from '../services/order.service';



@NgModule({
  declarations: [LoginComponent, CustomerTableComponent, NavBarComponent, CustomerModalComponent, ProductsListComponent, BuyComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[LoginComponent, CustomerTableComponent, NavBarComponent, CustomerModalComponent, ProductsListComponent, BuyComponent ],
  providers:[
    CustomersService,
    FormBuilder,
    ProductService,
    OrderService
  ]
})
export class ComponentsModule { }
